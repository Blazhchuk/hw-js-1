"use strict"

/* 
1. Змінні можна оголосити за допомогою: let, const, var. Але var застарів і його майже не використовують.
2.  string - це рядок який зазвичай присвоюється в лапках '', "", ``.
3. Перевірити можна за допомогою команди console.log(typeof).
4. Тому що '1' - це string а 1 - number, і тому js просто приєднав рядок до цифри .
*/

// Завдання 1
let num = 42;
console.log(typeof (num));

// Завдання 2
const name = 'Vlad';
const lastName = 'Blazhchuk';
console.log(`My name is ${name}, ${lastName}`);

// Завдання 3
let num2 = 24;
console.log(`My name is ${name} ${num2} ${lastName}`);
